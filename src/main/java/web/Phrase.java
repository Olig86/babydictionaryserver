package web;

public class Phrase {

    private String cooing;
    private String meaning;

    public Phrase(String cooing, String meaning) {
        this.cooing = cooing;
        this.meaning = meaning;
    }

    public String getCooing() {
        return cooing;
    }

    public String getMeaning() {
        return meaning;
    }
}
