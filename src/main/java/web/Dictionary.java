package web;


import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class Dictionary {

    private Map<String, String> phrases;

    public Dictionary() {

        Path pathToFile = Paths.get("words.txt");
        try (BufferedReader bufferedReader = Files.newBufferedReader(pathToFile)) {
            phrases = new HashMap<>();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] phrase = line.split(";");
                phrases.put(phrase[0], phrase[1]);
            }
        } catch (IOException e) {
            System.out.printf("there was a problem with file: %s", pathToFile);
        }
    }

    public Map<String, String> getPhrases() {
        return phrases;
    }
}
