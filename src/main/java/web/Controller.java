package web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;

@RestController
public class Controller {


    @RequestMapping("/checkMeaning")
    public String checkMeaning(@RequestParam(value="cooing") String cooing) {
        Map<String, String> phrases = Application.dictionary.getPhrases();
        if (phrases.containsKey(cooing)){
            return phrases.get(cooing);
        }
        else return String.format("W słowniku nie znajduje się gaworzyna %s%n",cooing);
    }

    @RequestMapping("/getList")
    public Set getList() {
        return Application.dictionary.getPhrases().keySet();
    }
}
