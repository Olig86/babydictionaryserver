package web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static Dictionary dictionary;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        dictionary = new Dictionary();
    }

}
